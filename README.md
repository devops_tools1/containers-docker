# Docker ejemplos

## ¿Qué es Docker?

<p style='text-align: justify;'> 
Esta es una plataforma para desarrollar y desplegar aplicaciones en contenedores. Docker provee una forma de aislar los componentes y dependencias de la aplicación en un contenedor que puede ser utilizado para desplegarse en infraestructura. Esta herramienta es rápida y utilizada para poder manejar el ciclo de vida de una aplicación y se integra bastante bien a procesos de CI/CD.
</p>

### Componentes de Docker

* Cliente y Servidor de Docker: La aplicación cliente permite dar instrucciones para la creación de contenedores. Las instrucciones son ejecutadas por el servidor de Docker, normalmente utilizamos el cliente y el servidor en el mismo **host** pero podemos utilizar servidores de docker remotos.

* Imágenes de Docker: las imagenes hace referencia a la descripción de los componentes que hacen una imagen de docker. Normalemente estas imágenes son construidas a partir de un **Dockerfile**.

* Contenedores de Docker: los contenedores son instancias de las imagenes de Docker, podemos correr uno o más contenedores de **Docker** de una misma imágen.

* Registro de Docker: el registro se refiere a un repositorio centralizado donde las imagenes de Docker son almacenadas, este repositorio puede contar con varias versiones de la misma imagen.


## Documentación de referencia

* [Docker, explicación](https://www.docker.com/resources/what-container)

* [Docker overview](https://docs.docker.com/get-started/overview/)


### Youtube Playlist

* [Playlist de video ejemplos](https://youtube.com/playlist?list=PL8Xnw9lMxPCGQnhQwntYQrRagyFAzrE52)
